﻿#include <iostream>
#include "Queue.h"

int main()
{
	try {
		QueueArray<int> queue1(7);
		for (int i = 0; i < 6; i++) {
			queue1.enQueue(i + 10);
		}
		std::cout << "The queue is empty: " << (queue1.isEmpty() ? "true" : "false") << std::endl;
		std::cout << "Queue 1:" << std::endl;
		for (int i = 0; i < 6; i++) {
			std::cout << queue1.deQueue() << " ";
		}
		std::cout << std::endl;
		std::cout << "The queue is empty: " << (queue1.isEmpty() ? "true" : "false") << std::endl;
		std::cout << std::endl;

		std::cout << "Testing OverFlow" << std::endl;
		QueueArray<int> queue2(5);
		for (int i = 0; i < 6; i++) {
			queue2.enQueue(i + 7);
		}
		std::cout << "Queue 2:" << std::endl;
		for (int i = 0; i < 6; i++) {
			std::cout << queue2.deQueue() << " ";
		}
		std::cout << std::endl;
	}
	catch (const QueueArray<int>::QueueOverflow& e) {
		std::cerr << "Error: " << e.getMessage().c_str() << std::endl;
	}
	std::cout << std::endl;

	try {
		std::cout << "Testing UnderFlow" << std::endl;
		QueueArray<int> queue3(7);
		for (int i = 0; i < 6; i++) {
			queue3.enQueue(i + 4);
		}
		std::cout << "Queue 3:" << std::endl;
		for (int i = 0; i < 8; i++) {
			std::cout << queue3.deQueue() << " ";
		}
		std::cout << std::endl;
	}
	catch (const QueueArray<int>::QueueUnderflow& e) {
		std::cerr << "Error: " << e.getMessage().c_str() << std::endl;
	}
	std::cout << std::endl;

	try {
		std::cout << "Testing WrongQueueSize (big size)" << std::endl;
		QueueArray<int> queue4(4000000000);
	}
	catch (const QueueArray<int>::WrongQueueSize& e) {
		std::cerr << "Error: " << e.getMessage().c_str() << std::endl;
	}
	std::cout << std::endl;

	try {
		std::cout << "Testing WrongQueueSize = 0" << std::endl;
		QueueArray<int> queue5(0);
	}
	catch (const QueueArray<int>::WrongQueueSize& e) {
		std::cerr << "Error: " << e.getMessage().c_str() << std::endl;
	}

	return 0;
}