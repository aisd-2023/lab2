﻿#include <iostream>
#include "Stack.h"

int main()
{
	try {
		StackArray<int> stack1(7);
		for (int i = 0; i < 7; i++) {
			stack1.push(i + 10);
		}
		std::cout << "The stack is empty: " << (stack1.isEmpty() ? "true" : "false") << std::endl;
		std::cout << "Stack 1:" << std::endl;
		for (int i = 0; i < 7; i++) {
			std::cout << stack1.pop() << " ";
		}
		std::cout << std::endl;
		std::cout << "The stack is empty: " << (stack1.isEmpty() ? "true" : "false") << std::endl;
		std::cout << std::endl;

		std::cout << "Testing OverFlow" << std::endl;
		StackArray<int> stack2(5);
		for (int i = 0; i < 6; i++) {
			stack2.push(i + 7);
		}
		std::cout << "Stack 2:" << std::endl;
		for (int i = 0; i < 6; i++) {
			std::cout << stack2.pop() << " ";
		}
		std::cout << std::endl;
	}
	catch (const StackArray<int>::StackOverflow& e) {
		std::cerr << "Error: " << e.getMessage().c_str() << std::endl;
	}
	std::cout << std::endl;

	try {
		std::cout << "Testing UnderFlow" << std::endl;
		StackArray<int> stack3(7);
		for (int i = 0; i < 7; i++) {
			stack3.push(i + 4);
		}
		std::cout << "Stack 3:" << std::endl;
		for (int i = 0; i < 8; i++) {
			std::cout << stack3.pop() << " ";
		}
		std::cout << std::endl;
	}
	catch (const StackArray<int>::StackUnderflow& e) {
		std::cerr << "Error: " << e.getMessage().c_str() << std::endl;
	}
	std::cout << std::endl;

	try {
		std::cout << "Testing WrongStackSize (big size)" << std::endl;
		StackArray<int> stack4(4000000000);
	}
	catch (const StackArray<int>::WrongStackSize& e) {
		std::cerr << "Error: " << e.getMessage().c_str() << std::endl;
	}
	std::cout << std::endl;

	try {
		std::cout << "Testing WrongStackSize = 0" << std::endl;
		StackArray<int> stack5(0);
	}
	catch (const StackArray<int>::WrongStackSize& e) {
		std::cerr << "Error: " << e.getMessage().c_str() << std::endl;
	}

	return 0;
}