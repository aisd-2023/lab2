#pragma once

template <class T>
class Stack
{
public:
	class StackException
	{
	public:
		StackException(const std::string& message);
		std::string getMessage() const;
	private:
		std::string exceptionMessage;
	};

	virtual ~Stack() {}
	virtual void push(const T& e) = 0;
	virtual T pop() = 0;
	virtual bool isEmpty() = 0;
};

template <class T>
class StackArray : public Stack<T>
{
public:
	class StackOverflow : public Stack<T>::StackException
	{
	public:
		StackOverflow();
	};

	class StackUnderflow : public Stack<T>::StackException
	{
	public:
		StackUnderflow();
	};

	class WrongStackSize : public Stack<T>::StackException
	{
	public:
		WrongStackSize();
	};

	StackArray(size_t size = 100);
	~StackArray();
	virtual void push(const T& e) override;
	virtual T pop() override;
	virtual bool isEmpty() override;

private:
	StackArray(const StackArray<T>& src);
	StackArray& operator=(const StackArray<T>& src);
	StackArray& operator=(StackArray<T>&& src);
	size_t depth;
	T* arr;
	size_t top;
};

template <class T>
Stack<T>::StackException::StackException(const std::string& message)
	: exceptionMessage(message)
{}

template <class T>
std::string Stack<T>::StackException::getMessage() const
{
	return exceptionMessage;
}

template <class T>
StackArray<T>::StackOverflow::StackOverflow()
	: Stack<T>::StackException("Stack Overflow")
{}

template <class T>
StackArray<T>::StackUnderflow::StackUnderflow()
	: Stack<T>::StackException("Stack Underflow")
{}

template <class T>
StackArray<T>::WrongStackSize::WrongStackSize()
	: Stack<T>::StackException("Wrong Stack Size")
{}

template <class T>
StackArray<T>::StackArray(size_t size)
{
	if (size == 0) {
		throw WrongStackSize();
	}
	else {
		try {
			arr = new T[size];
			depth = size;
			top = 0;
		}
		catch (std::bad_alloc) {
			throw WrongStackSize();
		}
	}
}

template <class T>
StackArray<T>::~StackArray()
{
	if (arr != nullptr) {
		delete[] arr;
	}
}

template <class T>
void StackArray<T>::push(const T& e)
{
	if (top < depth) {
		arr[top] = e;
		top++;
	}
	else {
		throw StackOverflow();
	}

}

template <class T>
T StackArray<T>::pop()
{
	if (!isEmpty()) {
		top--;
		T curr_elem = arr[top];
		return curr_elem;
	}
	else {
		throw StackUnderflow();
	}
	return 0;
}

template <class T>
bool StackArray<T>::isEmpty()
{
	if (top == 0) return true;
	else return false;
}

