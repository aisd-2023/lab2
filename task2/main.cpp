﻿#include <iostream>
#include "Stack.h"

bool checkBalanceBrackets(const char* text, const int maxDeep);int checkIsOpenBracket(const char symb);int checkIsCloseBracket(const char symb);

int main()
{
	const int numstr = 10;
	const int maxlen = 1000;
	try {
		char str[numstr][maxlen] = { "([4+8]-7)*4=20", 
			"(njn{37[c9[(fm", 
			"((fgws)){{{}}q}", 
			"[[hjwh]}edj]()", 
			"({dwl[5]}}fg))n", 
			"{[(}])", 
			"h)ve}cn])ejk}3c", 
			"bfjbfi)}ew{(", 
			"[{edk(()d)}]", 
			"()[]{}{([])}"};
		for (int i = 0; i < numstr; i++) {
			std::cout << "String " << i+1 << ": " << str[i] << std::endl;
			std::cout << "The string is correct with brackets: "
				<< (checkBalanceBrackets(str[i], 20) ? "true" : "false") << std::endl;
			std::cout << std::endl;
		}

		const int maxdep = 2;
		char str1[] = "(8j){(t[l]3g)}dfl";
		std::cout << "String 11: " << str1 << std::endl;
		std::cout << "The maximum depth of stack is " << maxdep << std::endl;
		std::cout << "The string is correct with brackets: "
			<< (checkBalanceBrackets(str1, maxdep) ? "true" : "false") << std::endl;
		std::cout << std::endl;
	}
	catch(const StackArray<int>::StackOverflow& e) {
		std::cerr << "Error: " << e.getMessage().c_str() << std::endl;
	}
	std::cout << std::endl;

	return 0;
}

bool checkBalanceBrackets(const char* text, const int maxDeep){	int i = 0;	int open_b = 0;	int close_b = 0;	StackArray<int> stackWithBrackets(maxDeep);	while (text[i] != '\0') {		open_b = checkIsOpenBracket(text[i]);		close_b = checkIsCloseBracket(text[i]);		if (open_b != 0) {			stackWithBrackets.push(open_b);		}		if (close_b != -1) {			if (stackWithBrackets.isEmpty()) return false;			else if (close_b != stackWithBrackets.pop()) return false;		}		i++;	}	if (!(stackWithBrackets.isEmpty())) return false;	return true;}int checkIsOpenBracket(const char symb){	if (symb == '(') return 1;	if (symb == '[') return 2;	if (symb == '{') return 3;	return 0;}int checkIsCloseBracket(const char symb){	if (symb == ')') return 1;	if (symb == ']') return 2;	if (symb == '}') return 3;	return -1;}